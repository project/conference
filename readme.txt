
----------------------------------------------------------------------
		       DRUPAL CONFERENCE MODULE
----------------------------------------------------------------------


I. Overview

  The Drupal conference module allows you to organise scholarly
  conferences with Drupal. It implements a mechanism to post papers,
  to manage reviewers and to assign reviewers to papers. A paper is an
  arbitrary node whith an attached file and a review is a arbitrary
  node assigned to a paper. It also takes care for the access
  permissions to papers and reviews:

    1. every author can only see his own paper
    2. every reviewer can only see his own reviews but download the
       files attached to the papers assigned to him
    3. a manager can see both, create assignments and make decision
       concerning a paper.
  
  The conference chair can make a decision concerning a paper based on
  all reviewes of that paper: 

    1. accept that paper 
    2. reject that paper
    3. request a modification

  He can also send reminders to all reviewers with outstanding papers.

  The conference module does not implement note types for posting
  papers and reviews. It is designed to cooperate with the content
  constriction kit. So you can provide a review questionnaire that
  suit your needs.

  There is one important point in this concept: The files assigned
  to a paper-node must not containt the author names!


II. Requirement

  a) Drupal 4.7 with MySQL-Database, Pgsql not supportet until know.

  b) The content construktion kit (cck module) to create content
     types.
  
  c) The upload module to allow file attachments to paper nodes.


III. Incompatibility

  Because the conference module implements a way to grant permissions
  to nodes, it is incompatible to modules that serve a node access
  mechanism, like:

    * node privacy byrole
    * simple access
    * taxonomy access
    * ...
  

IV. Installation and setup

  1. Download, install and setup the upload module and cck.

  2. Create node types for papers and reviews. Create roles for
     reviewers and for the conference chair. If you don't wish to use
     the "autenticated user" role for posting papers, create a third role.

  3. Allow uploading files (see admin/access) for the role dedicated to
     post papers and for the paper node type (see
     admin/settings/content-types). Ensure that all relevant file
     types are allowed (see admin/settings/upload).

     ATTENTION: This module has been tested only with public download
     method (see admin/settings). 

  4. Extract the conference modules's .tgz file in your drupal module
     directory. Then activate the module (see admin/modules).

  5. Choose the right roles and node types in
     admin/settings/conference. Consider to hide the title-field in
     the review creation form and to expand the file attachment
     fieldset in the paper creation form due to usability considerations.

  6. Enable the conference node permissions (see admin/settings/conference)

  7. Take care for the following access permissions (see admin/access):

    * reviewers must be able to edit own/create review nodes and to
      view uploaded files

    * the role dedicated to post papers must also be able to edit
      own/create paper nodes, to upload files and to view uploaded
      files.


VI. About

  This module is sponsored by the university of Duisburg-Essen
  (Germany) and developed for the german moodle conference 2007 (see
  http://www.moodle07.de) at the

    Chair of Educational Media and Knowledge Management
    (see http://www.mediendidaktik.de)

  by

    Tobias Hoelterhof
    (email: tobias [dot] hoelterhof [at] uni [minus] due [dot] de)

  Licence: GPL.